# Artefakt-förvaring

Denna konfiguration använder sig av [docker](https://www.docker.com/) och [docker-compose](https://docs.docker.com/compose/)
för att underlätta installation och konfiguration av ett system för [artefakt-förvaring](https://en.wikipedia.org/wiki/Software_repository).

De program som används för artefakt-förvaring är:
 
* [Nexus repository OSS](https://www.sonatype.com/nexus-repository-oss).  
  Som standard är namnet på administratörsanvändaren `admin` och ett slumpmässigt lösenord genereras vid första uppstart.
  Lösenordet kan visas genom följande kommando:
    ```shell script
    docker exec -it artefact-repository cat /nexus-data/admin.password; echo
    ```
* [NGINX](https://www.nginx.com/) används som [reverse-proxy](https://en.wikipedia.org/wiki/Reverse_proxy).

*I mina tester av denna konfiguration använder jag mig av [LXD](https://gitlab.com/snippets/1954551) för att sätta upp virtuella servrar.*

## Installation
```shell
cd /opt
```

```shell
git clone https://gitlab.com/jimmy.ljungberg/docker-artefact-repository.git artefact-repository
```

## Konfiguration
Port 443 används för att kommunicera med Nexus. Eventuell trafik på port 80 dirigieras om till port 443.

Dessutom har följande portar konfigurerats. Jag använder de för Docker-register:

* Port 10443 - docker-private
* Port 11443 - docker-public

[Behöver du generera ett eget (själv-signerat) certifikat?](https://gitlab.com/snippets/1963286)

```shell
cd /opt/artefact-repository
```

Skapa filen `.env`:

```script
SSL_CERTIFICATE=x
SSL_KEY=y
```

* Ersätt `x` med sökvägen till TLS-certifkat
* Ersätt `y` med sökvägen till privat nyckel för TLS-certifikat

#### Uppstart
```shell
docker-compose up -d
```

# Extra information
* [Konfigurera Nexus OSS](https://gitlab.com/snippets/1955892)
