events {
  worker_connections 1024;
}

http {
  proxy_send_timeout 120;
  proxy_read_timeout 300;
  proxy_buffering    off;
  keepalive_timeout  5 5;
  tcp_nodelay        on;

  server {
    listen *:80;
    return 301 https://$host$request_uri;
  }

  server {
    listen *:443 ssl;
    server_name artefact-repository.lxd;

    ssl_certificate      /etc/nginx/server.crt;
    ssl_certificate_key  /etc/nginx/server.key;

    client_max_body_size 1G;

    location / {
      proxy_pass http://artefact-repository:8081/;
      proxy_set_header Host $host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto "https";
    }
  }
}

stream {
  ssl_certificate      /etc/nginx/server.crt;
  ssl_certificate_key  /etc/nginx/server.key;

  upstream docker_private {
    server artefact-repository:10443;
  }

  upstream docker_public {
    server artefact-repository:11443;
  }

  server {
    listen 10443 ssl;
    proxy_pass docker_private;
  }

  server {
    listen 11443 ssl;
    proxy_pass docker_public;
  }
}
